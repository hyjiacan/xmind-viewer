# XMind Viewer

一个用于浏览器的简单的 XMind 预览组件。

<b>（注：展示风格与 XMind 完全不同，此组件仅解析出数据，并使用 antv/g6 以图的方式渲染出来）</b>

依赖

- jszip
- @antv/g6 *可选*

## 安装

```shell
npm install @hyjiacan/xmind-viewer --save
# 或者
yarn add @hyjiacan/xmind-viewer
```

## 用法

### 模块引用

```html
<style>
#app {
  width: 100vw;
  height: 100vh;
}
</style>
<div id="app"></div>
```

```js
import XMindViewer from '@hyjiacan/xmind-viewer'

const container = document.getElementById('app')

async function doRender() {
  const response = await fetch("/readme.xmind");
  const buffer = await response.arrayBuffer();
  // 直接使用 viewer 渲染界面
  const grapg = await XMindViewer.viewer.render(container)
}

doRender()
```

> 完整使用，参考文件 [main.js](main.js)

## 待办

- [ ] 备注的展示
- [ ] 标签的展示
- [ ] XML版本的样式处理
