class ThemeStyle {
  constructor() {
    this.type = "";
    this.styleId = "";
    this.properties = {};
  }

  static load(data) {
    const instance = new ThemeStyle();
    if (!data || !Object.keys(data).length) {
      return instance;
    }

    instance.type = data.type;
    instance.styleId = data.styleId;
    instance.properties = data.properties;
    return instance;
  }

  resolve() {
    const props = this.properties;
    if (!props) {
      return {};
    }
    // "svg:fill": "none",
    //       "border-line-width": "3",
    //       "border-line-color": "#F8F7F7",
    //       "line-color": "#CE6B94",
    //       "line-width": "3",
    //       "fo:font-family": "Raleway",
    //       "fo:font-weight": "900",
    //       "fo:font-style": "normal"
    const styles = {
      style: {},
      labelStyle: {},
    };
    this.fillStyle(styles.style, "border-line-color", "stroke");
    this.fillStyle(styles.style, "border-line-width", "lineWidth");

    this.fillStyle(styles.labelStyle, "fo:color", "fill");
    this.fillStyle(styles.labelStyle, "fo:font-family", "fontFamily");
    this.fillStyle(styles.labelStyle, "fo:font-weight", "fontWeight");
    this.fillStyle(styles.labelStyle, "fo:font-style", "fontstyle");
    return styles;
  }
  fillStyle(style, name, target) {
    const val = this.properties[name];
    if (!val) {
      return;
    }
    style[target] = val;
  }
}

class Theme {
  constructor() {
    this.id = "";
    /**
     * @type {ThemeStyle}
     */
    this.importantTopic = {};
    /**
     * @type {ThemeStyle}
     */
    this.minorTopic = {};
    /**
     * @type {ThemeStyle}
     */
    this.expiredTopic = {};
    /**
     * @type {ThemeStyle}
     */
    this.map = {};
    /**
     * @type {ThemeStyle}
     */
    this.centralTopic = {};
    /**
     * @type {ThemeStyle}
     */
    this.mainTopic = {};
    /**
     * @type {ThemeStyle}
     */
    this.subTopic = {};
    /**
     * @type {ThemeStyle}
     */
    this.boundary = {};
    /**
     * @type {ThemeStyle}
     */
    this.summary = {};
    /**
     * @type {ThemeStyle}
     */
    this.summaryTopic = {};
    /**
     * @type {ThemeStyle}
     */
    this.calloutTopic = {};
    /**
     * @type {ThemeStyle}
     */
    this.relationship = {};
    /**
     * @type {ThemeStyle}
     */
    this.floatingTopic = {};
  }

  /**
   *
   * @param {Object} data
   * @returns {Theme}
   */
  static load(data) {
    const instance = new Theme();
    if (!data) {
      return instance;
    }
    instance.id = data.id;
    instance.importantTopic = ThemeStyle.load(data.importantTopic);
    instance.minorTopic = ThemeStyle.load(data.minorTopic);
    instance.expiredTopic = ThemeStyle.load(data.expiredTopic);
    instance.map = ThemeStyle.load(data.map);
    instance.centralTopic = ThemeStyle.load(data.centralTopic);
    instance.mainTopic = ThemeStyle.load(data.mainTopic);
    instance.subTopic = ThemeStyle.load(data.subTopic);
    instance.boundary = ThemeStyle.load(data.boundary);
    instance.summary = ThemeStyle.load(data.summary);
    instance.summaryTopic = ThemeStyle.load(data.summaryTopic);
    instance.calloutTopic = ThemeStyle.load(data.calloutTopic);
    instance.relationship = ThemeStyle.load(data.relationship);
    instance.floatingTopic = ThemeStyle.load(data.floatingTopic);
    return instance;
  }
}

class Topic {
  constructor() {
    this.id = "";
    this.title = "";
    this.class = "";
    this.structureClass = "";
    this.children = {
      /**
       * @type {Array<Topic>}
       */
      attached: [],
    };
    this.branch = "";
    /**
     * @type {ThemeStyle}
     */
    this.style = null;

    this.notes = {
      realHTML: { content: "" },
      plain: { content: "" },
    };
    this.labels = [];
  }

  static load(data) {
    const instance = new Topic();
    if (!data) {
      return instance;
    }

    instance.id = data.id;
    instance.title = data.title;
    instance.class = data.class;
    instance.structureClass = data.structureClass;
    if (data.children) {
      instance.children.attached = data.children.attached.map((item) =>
        Topic.load(item)
      );
    }
    instance.branch = data.branch;

    instance.style = ThemeStyle.load(data.style);

    return instance;
  }
}

class Sheet {
  constructor() {
    this.id = "";
    this.title = "";
    this.class = "";
    /**
     * @type {Topic}
     */
    this.rootTopic = null;
    /**
     * @type {Theme}
     */
    this.theme = null;

    this.topicPositioning = "";
  }

  /**
   *
   * @param {Object} data
   * @returns {Sheet}
   */
  static load(data) {
    const instance = new Sheet();
    instance.id = data.id;
    instance.title = data.title;
    instance.class = data.class;
    instance.topicPositioning = data.topicPositioning;

    instance.rootTopic = Topic.load(data.rootTopic);
    instance.theme = Theme.load(data.theme);

    return instance;
  }
}

export { Theme, ThemeStyle, Topic, Sheet };
